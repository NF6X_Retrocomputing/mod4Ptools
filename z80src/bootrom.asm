;;; TRS-80 Model 4P boot ROM definitions:
;;;
;;; Formatted for use as an include file with zmac assembler.
;;; 
;;; Copyright 2013 by Mark J. Blair <nf6x@nf6x.net>
;;; Released under GPLv3
	list	-1

	;; Restart 0:
	;; Cold Start Boot
COLDBOOT macro
	RST	00h
	endm

	;; Restart 8:
	;; Disk I/O Request
DISKIO macro
	RST	08h
	endm

	;; Restart 10:
	;; Display string
DSTRING macro
	RST	10h
	endm

	;; Restart 18:
	;; Display block
DBLOCK macro
	RST	18h
	endm

	;; Restart 20:
	;; Byte Fetch (Called by Loader)
BFETCH macro
	RST	20h
	endm

	;; Restart 28:
	;; File Loader
FLOADER macro
	RST	28h
	endm

	;; Restart 30:
	;; Keyboard scanner
KEYSCAN	macro
	RST	30h
	endm

	;; Addresses of variables left in RAM for use by system software
ROMSEL	equ	401dh		; ROM Image selected: % for none, or A-G
BTYPE	equ	4055h		; Boot type: 1=floppy, 2=HD, 3=ARCNET, 4=RS-232C
SECSIZE	equ	4056h		; Boot sector size: 1=256, 2=512
BAUDRT	equ	4057h		; RS-232 Baud Rate (RS-232C boot only)
FNKEY	equ	4059h		; Function key selected: 86=F1/1, 87=F2/2,
				;   88-F3/3, 85=Caps, 84=Ctrl, 82=Lshift, 83=Rshift
BRKKEY	equ	405bh		; Break key indication: Non-zero if pressed
DTYPE	equ	405ch		; Disk type: 0=LDOS or TRSDOS 6, 1=TRSDOS 1.x
	
	list	1
	space	1
