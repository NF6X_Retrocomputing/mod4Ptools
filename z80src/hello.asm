;;; A small program for testing out TRS-80 Model 4P RS-232 boot mode.
;;; Prints "Hello, World!" at the top left corner of the Model 4P
;;; monitor. Runs in boot ROM context.
;;; 
;;; Copyright 2013 by Mark J. Blair <nf6x@nf6x.net>
;;; Released under GPLv3

	include bootrom.asm

	org	6000h
HELLO	ld	HL, HSTR
	ld	DE, 0
	DSTRING
	halt
FOREVR	jp	FOREVR
HSTR	defb	"Hello, World!", 00h
	end	HELLO
	
