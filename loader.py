#!/usr/bin/env python
#
# Copyright 2013 by Mark J. Blair <nf6x@nf6x.net>
# Released under GPLv3
#

"""loader.py: TRS-80 Model 4P Serial Bootloader.

    Copyright 2013 by Mark J. Blair <nf6x@nf6x.net>
    Released under GPLv3

    loader.py is a boot loader for transferring a software to the 4P
    over a serial connection using its RS-232 boot mode, which is
    invoked by holding down the right shift key while resetting the
    model 4P.

    Invocation example:

        ./loader.py /dev/cu.usbserial myprogram.hex

    In that example, /dev/cu.usbserial is a serial port which is
    connected to a model 4P, and myprogram.hex is the program to be
    transferred to the 4P. The progrma to be transferred must be in
    Intel hex format, with the execution start address specified in
    the EOF record. One way to generate such files is with the zmac
    cross-assembler, using its -h option."""


import sys
import serial
import time


defbaudrate = 19200
"""Default baud rate for connection to model 4P. 4P will auto-baud,
   with a maximum rate of 19200 bits per second."""

handshakedelay = 0.1
"""Delay between sent handshaking characters, in seconds."""


def usage():
    """Print usage instructions to stderr."""
    sys.stderr.write("Usage: \n")
    sys.stderr.write("  ./loader.py <serialport> <hexfile>\n")


def handshake(mod4p):
    """Establish a connection to the model 4P in RS-232 boot mode.

    Returns True after successful connection, or False if an error occurs."""

    print "Establishing connection to Model 4P computer:"
    print ""
    print "Put the Model 4P into RS-232 serial boot mode by holding down"
    print "its right shift key while resetting it. Continue holding the"
    print "right shift key until Model 4P prints \"Ready\" on the screen."
    print ""
    print "If the Model 4P prints \"Not Ready\", then check cabling."
    print ""
    print "Note that the Model 4P needs to see the Carrier Detect signal"
    print "asserted before it will begin the RS-232 boot process."
    print " "

    history = str()
    syncing = True
    synced  = False
    
    while syncing:
        # Send 0x55
        if history.find("Fo") == -1:
            # Send 0x55 byte unless it looks like we're receiving a response.
            mod4p.write("\x55")

        # Read reply, if any, and print it on stdout
        reply=mod4p.read()
        if len(reply) > 0:
            sys.stdout.write(reply)
            sys.stdout.flush()
            history = history + reply

        # Check for Found Baud Rate message from 4P
        if history.find("Found Baud Rate ") != -1:
            # 4P has detected us and responded. Send sync byte.
            sys.stdout.write("\n")
            mod4p.write("\xFF")
            history = str()
            while syncing:
                reply=mod4p.read()
                if len(reply) > 0:
                    sys.stdout.write(reply)
                    sys.stdout.flush()
                    history = history + reply
                if history.find("Loading") != -1:
                    # Success!
                    sys.stdout.write("\n")
                    syncing = False
                    synced  = True
                elif history.find("Error") != -1:
                    # Failure!
                    sys.stdout.write("\n")
                    syncing = False
                    synced  = False
                
        else:
            # Delay to pace 0x55 output unless it looks like we're
            # receiving a response
            if history.find("Fo") == -1:
                time.sleep(handshakedelay)

    return synced


def sendfile(mod4p, hexfile):
    """Send a hex file to the model 4P using its RS-232 boot protocol.

    Record checksums are presently ignored.
    Returns True after successful download, False after failure."""

    for line in hexfile:
        if (len(line) >= 11) and (line[0] == ":"):
            reclen  = int(line[1:3], 16)
            offset  = int(line[3:7], 16)
            rectype = int(line[7:9], 16)

            if len(line) < ((reclen * 2) + 11):
                # Line is not long enough for specified data length
                sys.stderr.write("Error: Short line in hex file.\n")
                sys.stderr.write("Offending line: \"")
                sys.stderr.write(line)
                sys.stderr.write("\"\n")
                return False

            if (rectype == 0):
                # data record
                sys.stdout.write("DATA %04x  " % offset)
                sys.stdout.flush()
                mod4p.write("\x01")
                mod4p.write(chr((reclen + 2) % 256))
                mod4p.write(chr(offset & 0xff))
                mod4p.write(chr(offset >> 8))
                m = 9
                for n in range(reclen):
                    sys.stdout.write(line[m:m+2] + " ")
                    sys.stdout.flush()
                    mod4p.write(chr(int(line[m:m+2],16)))
                    m += 2
                sys.stdout.write("\n")

            elif (rectype == 1):
                # EOF record with execution address
                sys.stdout.write("EXEC %04x\n" % offset)
                sys.stdout.flush()
                mod4p.write("\x02\x02")
                mod4p.write(chr(offset & 0xff))
                mod4p.write(chr(offset >> 8))

                return True

    # Should not get here
    sys.stderr.write("\nError: EOF record not found in hex file.\n")
    return False
                
if __name__ == "__main__":
    """Main entry point."""

    # Check argument count
    if len(sys.argv) != 3:
        usage()
        exit(1)

    # Open the hex file to be transferred
    hexfile = open(sys.argv[2], "r")

    # Open the serial port
    mod4p = serial.Serial(port=sys.argv[1], baudrate=defbaudrate,
                          bytesize=serial.EIGHTBITS, parity=serial.PARITY_ODD,
                          stopbits=serial.STOPBITS_TWO,
                          timeout=0, writeTimeout=None, interCharTimeout=None,
                          xonxoff=False, rtscts=True, dsrdtr=True)

    
    mod4p.flush()
    
    # Establish connection to model 4P
    if handshake(mod4p):

        # Send the file
        sendfile(mod4p, hexfile)

    else:
        sys.stderr.write("Could not establish connection to Model 4P.\n")
        exit(1)

    # All done!
    hexfile.close()
    mod4p.close()
    exit(0)
